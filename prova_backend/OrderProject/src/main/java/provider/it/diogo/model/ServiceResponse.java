package provider.it.diogo.model;

import java.util.List;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Diogo.Dias
 */
public class ServiceResponse {
    private PedidoVO order;
    private List<PedidoVO> orderList;

    public ServiceResponse(PedidoVO order) {
        this.order = order;
    }

    public List<PedidoVO> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<PedidoVO> orderList) {
        this.orderList = orderList;
    }

    
    public ServiceResponse() {
    }
    
    
    
}
