/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package provider.it.diogo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import provider.it.diogo.model.PedidoListarDto;
import provider.it.diogo.model.PedidoVO;
import provider.it.diogo.model.Status;
import provider.it.diogo.model.VencimentoPedidoDto;

/**
 *
 * @author Diogo.Dias
 */
@Api(
        tags = "Pedidos"
)
@RestController
public class PedidoResource {


	@ApiOperation(value = "Consultar Pedido",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping("pedidos/consultar/{idPedido}/")
        public ResponseEntity<PedidoVO> consultar(
		@PathVariable("idPedido") 	String idPedido) {
                /* Regra de Negocio*/
		return new ResponseEntity<PedidoVO>(new PedidoVO(),
                                                    new HttpHeaders(),
                                                    HttpStatus.OK);
	}

	@ApiOperation(value = "Listar Pedidos",
                        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
			consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping(value = "pedidos/listar/")		
	public ResponseEntity<List<PedidoVO>> listar(
		@RequestBody PedidoListarDto pedidoListarDto) {
                /* Regra de Negocio*/
		return new ResponseEntity<List<PedidoVO>>(new ArrayList<PedidoVO>(),
                                                    new HttpHeaders(),
                                                    HttpStatus.OK);
	}

	@ApiOperation(value = "Criar Pedido",
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PutMapping(value = "pedidos/criar/")	
	public ResponseEntity<?> criar(
		@RequestBody PedidoVO pedido) {
                /* Regra de Negocio*/
		return new  ResponseEntity<>(null,new HttpHeaders(),
                                                    HttpStatus.OK);
	}

	@ApiOperation(value = "Alterar Pedido")
	@PostMapping(value = "pedidos/alterar/{idPedido}",
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)		
	public ResponseEntity<?> alterar(
            @PathVariable("idPedido") String idPedido,
            @RequestBody PedidoVO pedido) {
		/* Regra de Negocio*/
		return new  ResponseEntity<>(null,new HttpHeaders(),
                                                    HttpStatus.OK);
	}
	

	@ApiOperation(value = "Excluir Pedido",
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@DeleteMapping(value = "pedidos/excluir/{idPedido}")	
	public ResponseEntity<?> excluir(
             @PathVariable("idPedido") String idPedido,
		@RequestBody PedidoVO pedido) {
		/* Regra de Negocio*/
		return new  ResponseEntity<>(null,new HttpHeaders(),
                                                    HttpStatus.OK);
	}

	@ApiOperation(value = "Alterar Status Pedido",
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value = "pedidos/atualizarStatus/{idPedido}/{status}")		
	public ResponseEntity<?> atualizarStatus(
            @PathVariable("idPedido") String idPedido,
            @PathVariable("status") Status status) {
            /* Regra de Negocio*/
            return new  ResponseEntity<>(null,new HttpHeaders(),
                                                    HttpStatus.OK);
	}

	@ApiOperation(value = "Alterar Vencimento Pedido",
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value = "pedidos/atualizarVencimento/{idPedido}")		
	public ResponseEntity<?> atualizarVencimentoPedido(
            @PathVariable("idPedido") String idPedido,
            @RequestBody VencimentoPedidoDto vencimentoPedidoDto) {
		/* Regra de Negocio*/
		return new  ResponseEntity<>(null,new HttpHeaders(),
                                                    HttpStatus.OK);
	}

}
