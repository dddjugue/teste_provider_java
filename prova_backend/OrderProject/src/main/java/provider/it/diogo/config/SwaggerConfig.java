package provider.it.diogo.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <i>Configuração do documento de apresentação dos serviços REST</i> <br />
 * 
 * @since 24/10/2019
 * @Version 1.0.0
 * @author Diogo Dias
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Autowired
	private SwaggerConfiguration sc;

	@Bean
	public Docket serviceApi() {
            return new Docket(SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage( "provider.it.diogo.controller" ))
                .paths(PathSelectors.any())
                .build().apiInfo(termosEServicos());
	}

	private ApiInfo termosEServicos() {
		return new ApiInfo(sc.getTitle(), sc.getDescription(), sc.getVersion(), sc.getTermsOfServiceUrl(),
				new Contact(sc.getName(), sc.getUrl(), sc.getEmail()), sc.getLicense(), sc.getLicenseUrl(),
				Collections.emptyList());
	}
        
        

}
