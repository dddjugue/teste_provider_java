/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package provider.it.diogo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author Diogo.Dias
 */
@EnableSwagger2
@SpringBootApplication
/*Apos executar abra o navegador e veja os endpoints no url http://localhost:8989/order-project-java/v1/swagger-ui.html#*/
public class OrderMain  {

    public static void main(String[] args){
    	 SpringApplication.run(OrderMain .class, args);
      }

}

