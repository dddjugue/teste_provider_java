/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package provider.it.diogo.model;

import java.util.Date;

/**
 *
 * @author Diogo.Dias
 */
public class VencimentoPedidoDto {
    private Date orderDueDate;

    public Date getOrderDueDate() {
        return orderDueDate;
    }

    public void setOrderDueDate(Date orderDueDate) {
        this.orderDueDate = orderDueDate;
    }
    
    
}
