const pokedex = document.getElementById('pokedex');
var comp = 1;
const fetchPokemon = (a) => {
	
    const promises = [];
	
        const url = `https://pokeapi.co/api/v2/pokemon/${a}`;
        promises.push(fetch(url).then((res) => res.json()));

    Promise.all(promises).then((results) => {
        const pokemon = results.map((result) => ({
            name: result.name,
            image: result.sprites['front_default'],
            type: result.types.map((type) => type.type.name).join(', '),
            id: result.id,
			weight: result.weight/10.0,
			height: result.height/10.0,
			base_experience: result.base_experience,
			abilities: result.abilities.map((ability) => ability.ability.name).join(', '),
			moves: result.moves.map((move) => move.move.name).join(', '),
			stats: result.stats.map((stat) => stat.stat.name + ': ' +stat.base_stat ).join('<br> ')
			//stats2: result.stats.map((stat) => ' <input type="range" min="1" max="100" value="10"> ' ).join(' ')
        }));
        displayPokemon(pokemon);
    });
};

const displayPokemon = (pokemon) => {
    console.log(pokemon);
    const pokemonHTMLString = pokemon
        .map(
            (pokeman) => `
        <li class="card">
            <img class="card-image" src="${pokeman.image}"/>
            <h2 class="card-title">${pokeman.id}. ${pokeman.name}</h2>
            <p class="card-subtitle">Type: ${pokeman.type}</p>
			<p class="card-subtitle">Height: ${pokeman.height} m</p>
			<p class="card-subtitle">Weight: ${pokeman.weight} Kg</p>
			<p class="card-subtitle">Stats:<br> ${pokeman.stats} </p>		
			<p class="card-subtitle">Base Exp: ${pokeman.base_experience} xp</p>
			<p class="card-subtitle">Abilities: ${pokeman.abilities}</p>
			<p class="card-subtitle">Moves: ${pokeman.moves}</p>
			<a href= "ListaPokemon.html"> Voltar</a>
			
        </li>	
    ` 
        )
        .join('');
    pokedex.innerHTML = pokemonHTMLString;
};

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return 1;
    if (!results[2]) return 1;
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

comp = getParameterByName('id');
fetchPokemon(comp);


