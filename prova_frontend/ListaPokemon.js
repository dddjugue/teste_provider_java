const pokedex = document.getElementById('pokedex');

const fetchPokemon = (a) => {
    const promises = [];
	var max = 0;
	if(a == 5){
		max = 893;
	}else{
		max  = 151+(a*151);
	}
    for (let i = 1+(a*151); i <= max; i++) {
        const url = `https://pokeapi.co/api/v2/pokemon/${i}`;
        promises.push(fetch(url).then((res) => res.json()));
    }
    Promise.all(promises).then((results) => {
        const pokemon = results.map((result) => ({
            name: result.name,
            image: result.sprites['front_default'],
            id: result.id
        }));
        displayPokemon(pokemon);
    });
};

const displayPokemon = (pokemon) => {
    console.log(pokemon);
    const pokemonHTMLString = pokemon
        .map(
            (pokeman) => `
        <li class="card">
            <img class="card-image" src="${pokeman.image}"/>
            <h2 class="card-title">${pokeman.id}. ${pokeman.name}</h2>
            <a href= "EstiloAtributosPokemon.html?id=${pokeman.id}">Stats</a>
        </li>
    `
        )
        .join('');
    pokedex.innerHTML = pokemonHTMLString;
};

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return 0;
    if (!results[2]) return 0;
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

comp = getParameterByName('page');
fetchPokemon(comp);
